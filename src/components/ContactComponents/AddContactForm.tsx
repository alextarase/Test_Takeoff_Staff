import React, { FC, useState } from "react";
import { Button, Form, Input, InputNumber, Row } from "antd";
import { IContact } from "../../models/IContact";
import { rules } from "../../utils/rules";

interface ContactFormProps {
  contacts: IContact[];
  submit: (event: IContact) => void;
}

const AddContactForm: FC<ContactFormProps> = ({ submit }) => {
  const [contact, setContact] = useState<IContact>({
    id: 0,
    name: "",
    username: "",
    email: "",
    phone: "",
    website: "",
  });

  const submitForm = () => {
    submit({
      id: contact.id,
      name: contact.name,
      username: contact.username,
      email: contact.email,
      phone: contact.phone,
      website: contact.website,
    });
  };

  return (
    <Form onFinish={submitForm}>
      <Form.Item
        label="Id контакта"
        name="id"
        rules={[rules.required("Пожалуйста, введиет id!")]}
      >
        <InputNumber
          onChange={(e) => setContact((prev) => ({ ...prev, id: e }))}
          value={contact.id}
          placeholder="Id"
        />
      </Form.Item>

      <Form.Item
        label="Имя контакта"
        name="name"
        rules={[rules.required("Пожалуйста, введиет имя!")]}
      >
        <Input
          id="1"
          onChange={(e) =>
            setContact((prev) => ({ ...prev, name: e.target.value }))
          }
          value={contact.name}
          placeholder="Пожалуйста, введиет имя"
        />
      </Form.Item>

      <Form.Item
        label="Телефон контакта"
        name="phone"
        rules={[rules.required("Пожалуйста, введиет телефон!")]}
      >
        <Input
          id="2"
          onChange={(e) =>
            setContact((prev) => ({ ...prev, phone: e.target.value }))
          }
          value={contact.phone}
          placeholder="Пожалуйста, введиет телефон"
        />
      </Form.Item>

      <Form.Item label="Ник контакта" name="username">
        <Input
          id="3"
          onChange={(e) =>
            setContact((prev) => ({ ...prev, username: e.target.value }))
          }
          value={"contact.username"}
          placeholder="Пожалуйста, введиет ник"
        />
      </Form.Item>

      <Form.Item label="Почта контакта" name="email">
        <Input
          onChange={(e) =>
            setContact((prev) => ({ ...prev, email: e.target.value }))
          }
          value={contact.email}
          placeholder="Пожалуйста, введиет почту"
        />
      </Form.Item>

      <Form.Item label="Сайт контакта" name="site">
        <Input
          onChange={(e) =>
            setContact((prev) => ({ ...prev, website: e.target.value }))
          }
          value={contact.website}
          placeholder="Пожалуйста, введиет сайт"
        />
      </Form.Item>

      <Row justify="end">
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Создать контакт
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
};

export default AddContactForm;
