import { Button, Modal } from "antd";
import React, { FC, useState } from "react";
import { useActions } from "../../hooks/useActions";
import { IContact } from "../../models/IContact";
import "../../App";
import EditContactForm from "./EditContactForm";

interface ContactProps {
  contacts: IContact;
}

const ContactItem: FC<ContactProps> = (props) => {
  const { deleteContactAsync, editeContactAsync } = useActions();
  const [modal, setModal] = useState(false);

  const deleteContacts = () => {
    const confirm = window.confirm("Вы действительно хотите удалить контакт?");
    if (confirm) {
      deleteContactAsync(props.contacts.id);
    }
  };

  const editNewContact = (contact: IContact) => {
    setModal(false);
    editeContactAsync(contact);
  };

  const handleEditButtonClick = () => {
    setModal(true);
  };

  return (
    <div className="contact">
      <div className="contact__content">
        <h3>Имя: {props.contacts.name}</h3>
        <h4>Номер телефона: {props.contacts.phone}</h4>
        <h4>Электронная почта: {props.contacts.email}</h4>
        <h4>Сайт: {props.contacts.website}</h4>
        <div className="card__buttons">
          <Button
            className="card__button"
            onClick={() => deleteContacts()}
            type="primary"
            danger
          >
            Удалить
          </Button>
          <Modal
            title="Изменить контакт"
            visible={modal}
            footer={null}
            onCancel={() => setModal(false)}
          >
            <EditContactForm
              contacts={props.contacts}
              submit={editNewContact}
            ></EditContactForm>
          </Modal>
          <Button
            className="card__button"
            onClick={handleEditButtonClick}
            type="primary"
          >
            Изменить
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ContactItem;
