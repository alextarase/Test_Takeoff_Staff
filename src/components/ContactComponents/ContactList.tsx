import React, { FC } from "react";
import ContactItem from "./ContactItem";
import { IContact } from "./../../models/IContact";

interface ContactProps {
  contacts: IContact[];
}

const ContactList: FC<ContactProps> = (props) => {
  return (
    <>
      {props.contacts.map((contact) => (
        <ContactItem key={contact.id} contacts={contact} />
      ))}
    </>
  );
};

export default ContactList;
