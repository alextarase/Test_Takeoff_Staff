import { Button, Input, Layout, Modal, Typography } from "antd";
import AddContactForm from "../components/ContactComponents/AddContactForm";
import ContactList from "../components/ContactComponents/ContactList";
import { useActions } from "../hooks/useActions";
import { useTypedselector } from "../hooks/useTypedSelector";
import { IContact } from "../models/IContact";
import { FC, useEffect, useState } from "react";

const Contacts: FC = () => {
  const { contacts } = useTypedselector((state) => state.contacts);
  const { fetchContact, createContact } = useActions();
  const [value, setVelue] = useState("");
  const [modal, setModal] = useState(false);

  const { Title } = Typography;

  useEffect(() => {
    fetchContact();
  }, []);

  const filteredContacts = contacts.filter((contact) => {
    return contact.name.toLowerCase().includes(value.toLowerCase());
  });

  const addNewContact = (contact: IContact) => {
    setModal(false);
    createContact(contact);
  };

  const handleModalCancel = () => {
    setModal(false);
  };

  const handleModalOpen = () => {
    setModal(true);
  };

  return (
    <Layout>
      <Title level={1} className="mainTitle">
        Список контактов
      </Title>
      <Input
        placeholder="Поиск контакта..."
        onChange={(e) => setVelue(e.target.value)}
        className="searchInput"
      />
      <Button className="addButton" type="default" onClick={handleModalOpen}>
        Добавить контакт
      </Button>
      <Modal
        title="Добавить контакт"
        visible={modal}
        footer={null}
        onCancel={handleModalCancel}
      >
        <AddContactForm contacts={contacts} submit={addNewContact} />
      </Modal>
      <ContactList contacts={filteredContacts} />;
    </Layout>
  );
};

export default Contacts;
